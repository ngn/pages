
function ImageButtonModel() {
    this.isPressed = false;
    this.listeners = [];
    this.actionListeners = [];
}

ImageButtonModel.prototype.addListener = function (listener) {
    this.listeners.push(listener);
};

ImageButtonModel.prototype.removeListener = function (listener) {
    for (var i = this.listeners.length - 1; i >= 0; i--) {
        if (this.listeners[i] == listener) {
            this.listeners[i] = this.listeners[this.listeners.length - 1];
            this.listeners.length--;
            return;
        }
    }
};

ImageButtonModel.prototype.fire = function () {
    for (var i = this.listeners.length - 1; i >= 0; i--) {
        this.listeners[i].modelChanged();
    }
};

ImageButtonModel.prototype.addActionListener = function (listener) {
    this.actionListeners.push(listener);
};

ImageButtonModel.prototype.removeActionListener = function (listener) {
    for (var i = this.actionListeners.length - 1; i >= 0; i--) {
        if (this.actionListeners[i] == listener) {
            this.actionListeners[i] = this.actionListeners[this.actionListeners.length - 1];
            this.actionListeners.length--;
            return;
        }
    }
};

ImageButtonModel.prototype.fireAction = function () {
    for (var i = this.actionListeners.length - 1; i >= 0; i--) {
        this.actionListeners[i].actionPerformed();
    }
};

ImageButtonModel.prototype.setPressed = function (b) {
    if (this.isPressed == b) {
        return;
    }
    this.isPressed = b;
    this.fire();
};

ImageButtonModel.prototype.setNormalImage = function (x) {
    if (this.normalImage == x) {
        return;
    }
    this.normalImage = x;
    this.fire();
};

ImageButtonModel.prototype.setPressedImage = function (x) {
    if (this.pressedImage == x) {
        return;
    }
    this.pressedImage = x;
    this.fire();
};
