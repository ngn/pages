
var IconManager = {};

var X_IMAGE_CELL_0           =  1;
var X_IMAGE_CELL_1           =  2;
var X_IMAGE_CELL_2           =  3;
var X_IMAGE_CELL_3           =  4;
var X_IMAGE_CELL_4           =  5;
var X_IMAGE_CELL_5           =  6;
var X_IMAGE_CELL_6           =  7;
var X_IMAGE_CELL_7           =  8;
var X_IMAGE_CELL_8           =  9;
var X_IMAGE_CELL_MARKED      = 10;
var X_IMAGE_CELL_MINE        = 11;
var X_IMAGE_CELL_RED         = 12;
var X_IMAGE_CELL_UNKNOWN     = 13;
var X_IMAGE_CELL_WRONG       = 14;
var X_IMAGE_DIGIT_0          = 15;
var X_IMAGE_DIGIT_1          = 16;
var X_IMAGE_DIGIT_2          = 17;
var X_IMAGE_DIGIT_3          = 18;
var X_IMAGE_DIGIT_4          = 19;
var X_IMAGE_DIGIT_5          = 20;
var X_IMAGE_DIGIT_6          = 21;
var X_IMAGE_DIGIT_7          = 22;
var X_IMAGE_DIGIT_8          = 23;
var X_IMAGE_DIGIT_9          = 24;
var X_IMAGE_DIGIT_MINUS      = 25;
var X_IMAGE_SMILEY           = 26;
var X_IMAGE_SMILEY_DEAD      = 27;
var X_IMAGE_SMILEY_PENDING   = 28;
var X_IMAGE_SMILEY_PRESSED   = 29;
var X_IMAGE_SMILEY_WINNER    = 30;
var X_IMAGE_WINMINE          = 31;


var IMAGE_SRC = [
    "",
    "images/cell_0.gif",
    "images/cell_1.gif",
    "images/cell_2.gif",
    "images/cell_3.gif",
    "images/cell_4.gif",
    "images/cell_5.gif",
    "images/cell_6.gif",
    "images/cell_7.gif",
    "images/cell_8.gif",
    "images/cell_marked.gif",
    "images/cell_mine.gif",
    "images/cell_red.gif",
    "images/cell_unknown.gif",
    "images/cell_wrong.gif",
    "images/digit_0.gif",
    "images/digit_1.gif",
    "images/digit_2.gif",
    "images/digit_3.gif",
    "images/digit_4.gif",
    "images/digit_5.gif",
    "images/digit_6.gif",
    "images/digit_7.gif",
    "images/digit_8.gif",
    "images/digit_9.gif",
    "images/digit_minus.gif",
    "images/smiley.gif",
    "images/smiley_dead.gif",
    "images/smiley_pending.gif",
    "images/smiley_pressed.gif",
    "images/smiley_winner.gif",
    "images/winmine.gif",
];


IconManager.setSrc = function (e, x) {
    if (e.srcX != x) {
        e.srcX = x;
        e.src = IMAGE_SRC[x];
    }
};
