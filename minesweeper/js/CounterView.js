
function CounterView(document, model) {
    this.model = model;
    this.eTable = document.createElement("TABLE");
    this.eTable.style.borderStyle = "Solid";
    this.eTable.style.borderWidth = 1;
    this.eTable.style.borderColor = "#808080 #ffffff #ffffff #808080";
    this.eTable.cellSpacing = this.eTable.cellPadding = 0;
    var eTBody = document.createElement("TBODY");
    this.eTable.appendChild(eTBody);
    var eTR = document.createElement("TR");
    eTBody.appendChild(eTR);
    this.eImgs = new Array(3);
    for (var i = 0; i < 3; i++) {
        var eTD = document.createElement("TD");
        eTR.appendChild(eTD);
        this.eImgs[i] = document.createElement("IMG");
        eTD.appendChild(this.eImgs[i]);
    }
    model.addListener(this);
    this.modelChanged();
}

CounterView.prototype.modelChanged = function () {
    var v = this.model.getValue();
    var isNegative = (v < 0);
    if (isNegative) {
        v = -v;
        IconManager.setSrc(this.eImgs[0], X_IMAGE_DIGIT_MINUS);
    }
    var limit = (isNegative) ? 1 : 0;
    for (var i = 2; i >= limit; i--) {
        IconManager.setSrc(this.eImgs[i], X_IMAGE_DIGIT_0 + (v % 10));
        v = Math.floor(v / 10);
    }
};

CounterView.prototype.getDOM = function () {
    return this.eTable;
};
