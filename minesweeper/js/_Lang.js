

var isNS = (navigator.userAgent.indexOf("Gecko") != -1);

var FALSE_FUNCTION = function () {
    return false;
};

// General-purpose functions

function getAllElementsById() {
    getAllElementsById0(document);
};

function getAllElementsById0(e) {
    if (e.id != null) {
        window[e.id] = e;
    }
    for (var i = e.firstChild; i != null; i = i.nextSibling) {
        getAllElementsById0(i);
    }
};

function removeAllChildren(e) {
    while (e.firstChild != null) {
        e.removeChild(e.firstChild);
    }
};
