

function MineSweeperView(document, model) {
    this.model = model;
    model.addListener(this);
    this.buttonStatus = 0; // 2 bits representing each mouse button (1-left, 2-right)
    // Create the TABLE of cells
    var eTableGrid = document.createElement("TABLE");
    eTableGrid.cellSpacing = eTableGrid.cellPadding = 0;
    this.eTBody = this.eTBody = document.createElement("TBODY");
    eTableGrid.appendChild(this.eTBody);
    this.eTRs = new Array(model.nRows);
    this.eTDs = new Array(model.nRows);
    this.eImgs = new Array(model.nRows);
    for (var i = 0; i < model.nRows; i++) {
        var eTR = this.eTRs[i] = document.createElement("TR");
        this.eTDs[i] = new Array(model.nCols);
        this.eImgs[i] = new Array(model.nCols);
        for (var j = 0; j < model.nCols; j++) {
            var eTD = this.eTDs[i][j] = document.createElement("TD");
            var eImg = this.eImgs[i][j] = document.createElement("IMG");
            eImg.row = i;
            eImg.col = j;
            eTD.appendChild(eImg);
            eTR.appendChild(eTD);
        }
        this.eTBody.appendChild(eTR);
    }
    // Create smiley button and counters
    this.smileyButton = new ImageButtonView(
            document,
            model.getSmileyButtonModel(),
            X_IMAGE_SMILEY,
            X_IMAGE_SMILEY_PRESSED
    );
    var remainingMinesCounterView = new CounterView(document, this.model.getRemainingMinesCounterModel());
    var clockCounterView = new CounterView(document, this.model.getClockCounterModel());
    // Wrap everything in a table
    var eOuterTable = document.createElement("TABLE");
    eOuterTable.style.backgroundColor = "#c0c0c0";
    eOuterTable.cellSpacing = 6;
    eOuterTable.cellPadding = 0;
    var eOuterTBody = document.createElement("TBODY");
    eOuterTable.appendChild(eOuterTBody);
    var eOuterTR0 = document.createElement("TR");
    eOuterTBody.appendChild(eOuterTR0);
    var eOuterTD00 = document.createElement("TD");
    eOuterTR0.appendChild(eOuterTD00);
    var eOuterTR1 = document.createElement("TR");
    eOuterTBody.appendChild(eOuterTR1);
    var eOuterTD10 = document.createElement("TD");
    eOuterTR1.appendChild(eOuterTD10);
    // Create table of cells
    var eInnerTable = document.createElement("TABLE");
    eInnerTable.style.padding = "1 4 0 2";
    eInnerTable.cellSpacing = 3;
    eInnerTable.cellPadding = 0;
    eInnerTable.style.borderStyle = "solid";
    eInnerTable.style.borderWidth = 2;
    eInnerTable.style.borderColor = "#808080 #ffffff #ffffff #808080";
    eInnerTable.style.width = "100%";
    eOuterTD00.appendChild(eInnerTable);
    var eInnerTBody = document.createElement("TBODY");
    eInnerTable.appendChild(eInnerTBody);
    var eInnerTR0 = document.createElement("TR");
    eInnerTBody.appendChild(eInnerTR0);
    var eInnerTD00 = document.createElement("TD");
    eInnerTD00.style.width = "40px";
    eInnerTR0.appendChild(eInnerTD00);
    var eInnerTD01 = document.createElement("TD");
    eInnerTD01.align = "center";
    eInnerTR0.appendChild(eInnerTD01);
    var eInnerTD02 = document.createElement("TD");
    eInnerTD02.style.width = "40px";
    eInnerTR0.appendChild(eInnerTD02);
    //
    eInnerTD00.appendChild(remainingMinesCounterView.getDOM());
    eInnerTD01.appendChild(this.smileyButton.getDOM());
    eInnerTD02.appendChild(clockCounterView.getDOM());
    eTableGrid.style.borderStyle = "solid";
    eTableGrid.style.borderWidth = 3;
    eTableGrid.style.borderColor = "#808080 #ffffff #ffffff #808080";
    eOuterTD10.appendChild(eTableGrid);
    this.eDOM = eOuterTable;
    this.eDOM.tabIndex = 1;
    this.eDOM.view = this;
    // Attach mouse listeners
    eTableGrid.view = this;
    eTableGrid.onmousemove = function (event0) {
        var model = this.view.model;
        if ((model.mode == MODE_WINNER) || (model.mode == MODE_DEAD)) {
            return;
        }
        var e = (isNS) ? event0.target : event.srcElement;
        if ((e == null) || (e.tagName != "IMG") || (e.row == null) || (e.col == null)) {
            model.setSelectedCell(-1, -1);
        } else {
            model.setSelectedCell(e.row, e.col);
        }
    }
    eTableGrid.onmousedown = function (event0) {
        var buttonDelta;
        if (isNS) {
            buttonDelta = (event0.button == 0) ? 1 : (event0.button == 2) ? 2 : 0;
        } else {
            buttonDelta = (event.button & 3) & ~this.view.buttonStatus;
        }
        this.view.buttonStatus ^= buttonDelta;
        var model = this.view.model;
        if ((model.mode == MODE_WINNER) || (model.mode == MODE_DEAD)) {
            return false;
        }
        if (this.view.buttonStatus == 2) {
            if (model.selectedRow != -1) {
                model.markCell(model.selectedRow, model.selectedCol);
            }
        } else if (this.view.buttonStatus == 1) {
            model.setMode(MODE_PENDING_CELL);
        } else if (this.view.buttonStatus == 3) {
            model.setMode(MODE_PENDING_NEIGHBOURS);
        } else if (this.view.buttonStatus == 0) {
            model.setMode(MODE_NORMAL);
        }
        return false;
    }
    eTableGrid.onmouseup = function (event0) {
        var buttonDelta;
        if (isNS) {
            buttonDelta = (event0.button == 0) ? 1 : (event0.button == 2) ? 2 : 0;
        } else {
            buttonDelta = (event.button & 3);
        }
        var oldButtonStatus = this.view.buttonStatus;
        this.view.buttonStatus ^= buttonDelta;
        var model = this.view.model;
        if ((model.mode == MODE_WINNER) || (model.mode == MODE_DEAD)) {
            return;
        }
        if ((oldButtonStatus & 1) != 0) {
            if (model.mode == MODE_PENDING_CELL) {
                if (model.selectedRow != -1) {
                    model.openCell(model.selectedRow, model.selectedCol);
                }
            } else if (model.mode == MODE_PENDING_NEIGHBOURS) {
                if (model.selectedRow != -1) {
                    model.openNeighboursOf(model.selectedRow, model.selectedCol);
                }
            }
        }
        if ((model.mode != MODE_WINNER) && (model.mode != MODE_DEAD)) {
            model.setMode(MODE_NORMAL);
        }
        return false;
    }
    // Attach keyboard listeners
    var this0 = this;
    document.onkeydown = function (event0) {
        var ch = (isNS) ? event0.keyCode : event.keyCode;
        if (ch == 27) { // ESCAPE
            this0.eDOM.view.model.stopTimer();
        } else if (ch == 113) { // F2
            this0.eDOM.view.model.reset();
        } else if (ch == 112) { // F1
            alert("Minesweeper in DHTML\nCopyright (C) 2004 Nick Nickolov");
        } else if (ch == 83) {
            this0.eDOM.view.model.solve();
        }
        return false;
    }
    if (!isNS) {
        this.eDOM.ondragstart = FALSE_FUNCTION;
    }
    this.eDOM.oncontextmenu = FALSE_FUNCTION;
    // Initial rendering
    this.modelChanged();
};

MineSweeperView.prototype.modelChanged = function () {
    // Repaint
    if ((this.model.mode == MODE_DEAD) || (this.model.mode == MODE_WINNER)) {
        for (var i = this.model.nRows - 1; i >= 0; i--) {
            for (var j = this.model.nCols - 1; j >= 0; j--) {
                var cell = this.model.cells[i][j];
                IconManager.setSrc(this.eImgs[i][j],
                        (cell.isMarked)
                                ? ((cell.isMine) ? X_IMAGE_CELL_MARKED : X_IMAGE_CELL_WRONG)
                                : ((cell.isMine)
                                        ? X_IMAGE_CELL_MINE
                                        : ((cell.isOpen)
                                                ? (X_IMAGE_CELL_0 + cell.nNeighbours)
                                                : X_IMAGE_CELL_UNKNOWN))
                );
            }
        }
        if (this.model.mode == MODE_DEAD) {
            var redCell = this.model.redCell;
            IconManager.setSrc(this.eImgs[redCell.row][redCell.col], X_IMAGE_CELL_RED);
            this.smileyButton.setNormalImage(X_IMAGE_SMILEY_DEAD);
        } else {
            this.smileyButton.setNormalImage(X_IMAGE_SMILEY_WINNER);
        }
    } else {
        for (var i = this.model.nRows - 1; i >= 0; i--) {
            for (var j = this.model.nCols - 1; j >= 0; j--) {
                var cell = this.model.cells[i][j];
                IconManager.setSrc(this.eImgs[i][j],
                        (cell.isMarked) ? X_IMAGE_CELL_MARKED :
                        (!cell.isOpen) ? X_IMAGE_CELL_UNKNOWN :
                        (cell.isMine) ? X_IMAGE_CELL_MINE :
                        (X_IMAGE_CELL_0 + cell.nNeighbours)
                );
            }
        }
        if ((this.model.mode != MODE_NORMAL) && (this.model.selectedRow != -1) && (this.model.selectedCol != -1)) {
            var cell = this.model.cells[this.model.selectedRow][this.model.selectedCol];
            var selectedImg = this.eImgs[this.model.selectedRow][this.model.selectedCol];
            IconManager.setSrc(selectedImg,
                    (cell.isMarked) ? X_IMAGE_CELL_MARKED :
                    (!cell.isOpen) ? X_IMAGE_CELL_0 :
                    (X_IMAGE_CELL_0 + cell.nNeighbours)
            );
            if (this.model.mode == MODE_PENDING_NEIGHBOURS) {
                for (var k = 0; k < 8; k++) {
                    var row1 = this.model.selectedRow + DX[k];
                    var col1 = this.model.selectedCol + DY[k];
                    if ((row1 >= 0) && (row1 < this.model.nRows) && (col1 >= 0) && (col1 < this.model.nCols)) {
                        var cell = this.model.cells[row1][col1];
                        var selectedImg = this.eImgs[row1][col1];
                        IconManager.setSrc(selectedImg,
                                (cell.isMarked) ? X_IMAGE_CELL_MARKED :
                                (!cell.isOpen) ? X_IMAGE_CELL_0 :
                                (X_IMAGE_CELL_0 + cell.nNeighbours)
                        );
                    }
                }
            }
            this.smileyButton.setNormalImage(X_IMAGE_SMILEY_PENDING);
        } else {
            this.smileyButton.setNormalImage(X_IMAGE_SMILEY);
        }
    }
};

MineSweeperView.prototype.getDOM = function () {
    return this.eDOM;
};
