
function CounterModel() {
    this.value = 0;
    this.listeners = [];
}

CounterModel.prototype.getValue = function () {
    return this.value;
};

CounterModel.prototype.setValue = function (value) {
    this.value = value;
    this.fire();
};

CounterModel.prototype.increment = function () {
    this.setValue(this.value + 1);
};

CounterModel.prototype.decrement = function () {
    this.setValue(this.value - 1);
};

CounterModel.prototype.addListener = function (listener) {
    this.listeners[this.listeners.length] = listener;
};

CounterModel.prototype.removeListener = function (listener) {
    for (var i = this.listeners.length - 1; i >= 0; i--) {
        if (this.listeners[i] == listener) {
            this.listeners[i] = this.listeners[this.listeners.length - 1];
            this.listeners.length--;
        }
    }
};

CounterModel.prototype.fire = function () {
    for (var i = this.listeners.length - 1; i >= 0; i--) {
        this.listeners[i].modelChanged(this);
    }
};
