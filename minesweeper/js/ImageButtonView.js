
function ImageButtonView(document, model, normalImage, pressedImage) {
    this.model = model;
    this.normalImage = normalImage;
    this.pressedImage = pressedImage;
    this.eImg = document.createElement("IMG");
    this.eImg.view = this;
    model.addListener(this);
    this.eImg.onmousedown = function (event0) {
        this.view.model.setPressed(true);
        return false;
    }
    this.eImg.onmouseout = function (event0) {
        this.view.model.setPressed(false);
        return false;
    }
    this.eImg.onmouseup = function (event0) {
        if (this.view.model.isPressed) {
            this.view.model.fireAction();
        }
        this.view.model.setPressed(false);
        return false;
    }
    this.eImg.ondragstart = function (event0) {
        return false;
    }
    this.eImg.oncontextmenu = FALSE_FUNCTION;
    this.repaint();
};

ImageButtonView.prototype.setNormalImage = function (x) {
    this.normalImage = x;
    this.repaint();
};

ImageButtonView.prototype.setPressedImage = function (x) {
    this.pressedImage = x;
    this.repaint();
};

ImageButtonView.prototype.modelChanged = function () {
    this.repaint();
};

ImageButtonView.prototype.repaint = function () {
    IconManager.setSrc(
        this.eImg,
        (this.model.isPressed) ? this.pressedImage : this.normalImage
    );
};

ImageButtonView.prototype.getDOM = function () {
    return this.eImg;
};
