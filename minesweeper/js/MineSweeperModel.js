
var DX = [ -1, -1, -1, 0, 1, 1, 1, 0 ];
var DY = [ -1, 0, 1, 1, 1, 0, -1, -1 ];

var MODE_NORMAL             = 1;
var MODE_PENDING_CELL       = 2; // The left mouse button is down
var MODE_PENDING_NEIGHBOURS = 3; // Both mouse buttons are down
var MODE_DEAD               = 4; // Game over - success
var MODE_WINNER             = 5; // Game over - failure



function MineSweeperModel(configuration) {
    var nRows = this.nRows = configuration.nRows;
    var nCols = this.nCols = configuration.nCols;
    var nMines = this.nMines = configuration.nMines;
    if (nRows == null) {
        nRows = 9;
        nCols = 9;
        nMines = 10;
    }
    this.cells = new Array(nRows);
    for (var i = nRows - 1; i >= 0; i--) {
        this.cells[i] = new Array(nCols);
        for (var j = nCols - 1; j >= 0; j--) {
            this.cells[i][j] = new MineSweeperModelCell(i, j);
        }
    }
    this.redCell = null;
    this.selectedRow = 0;
    this.selectedCol = 0;
    this.mode = MODE_NORMAL;
    this.listeners = [];
    this.isVirgin = true;
    this.remainingMinesCounterModel = new CounterModel();
    this.clockCounterModel = new CounterModel();
    this.smileyButtonModel = new ImageButtonModel();
    this.smileyButtonModel.addActionListener(this);
    this.nUnknownCells = 0;
    this.intervalId = -1;
    this.reset();
};

MineSweeperModel.prototype.reset = function () {
    for (var i = this.nRows - 1; i >= 0; i--) {
        for (var j = this.nCols - 1; j >= 0; j--) {
            var cell = this.cells[i][j];
            cell.isMine = false;
            cell.isOpen = false;
            cell.isMarked = false;
            cell.nNeighbours = 0;
        }
    }
    this.mode = MODE_NORMAL;
    this.redCell = null;
    this.isVirgin = true;
    this.remainingMinesCounterModel.setValue(this.nMines);
    this.nUnknownCells = this.nRows * this.nCols;
    this.fire();
    this.stopTimer();
    this.clockCounterModel.setValue(0);
};

MineSweeperModel.prototype.stopTimer = function () {
    if (this.intervalId != -1) {
        clearInterval(this.intervalId);
        this.intervalId = -1;
    }
};

MineSweeperModel.prototype.randomizeMines = function (row, col) {
    // The arguments are the coordinates of the
    // currently opened cell which must not be a mine
    var mineCounter = this.nMines;
    var cellCounter = this.nRows * this.nCols - 1;
    for (var i = this.nRows - 1; i >= 0; i--) {
        for (var j = this.nCols - 1; j >= 0; j--) {
            if ((i != row) || (j != col)) {
                cellCounter--;
                if (Math.random() * cellCounter < mineCounter) {
                    this.cells[i][j].isMine = true;
                    mineCounter--;
                }
            }
        }
    }
    for (var i = this.nRows - 1; i >= 0; i--) {
        for (var j = this.nCols - 1; j >= 0; j--) {
            var cell = this.cells[i][j];
            if (cell.isMine) {
                for (var k = 7; k >= 0; k--) {
                    var i1 = i + DX[k];
                    var j1 = j + DY[k];
                    if ((i1 >= 0) && (i1 < this.nRows) && (j1 >= 0) && (j1 < this.nCols)) {
                        this.cells[i1][j1].nNeighbours++;
                    }
                }
            }
        }
    }
    this.stopTimer();
    this.clockCounterModel.increment();
    var this0 = this;
    this.intervalId = setInterval(function () {
        this0.clockCounterModel.increment();
        if (this0.clockCounterModel.getValue() >= 999) {
            this0.stopTimer();
        }
    }, 1000);
};

MineSweeperModel.prototype.openCell = function (row, col) {
    var cell = this.cells[row][col];
    if (this.isVirgin) {
        this.randomizeMines(row, col);
        this.isVirgin = false;
    }
    if (cell.isOpen || cell.isMarked) {
        return;
    }
    cell.isOpen = true;
    this.nUnknownCells--;
    if (cell.isMine) {
        this.mode = MODE_DEAD;
        this.redCell = cell;
        this.stopTimer();
        this.fire();
        return;
    }
    if (cell.nNeighbours == 0) {
        for (var k = 0; k < 8; k++) {
            var row1 = row + DX[k];
            var col1 = col + DY[k];
            if ((row1 >= 0) && (row1 < this.nRows) && (col1 >= 0) && (col1 < this.nCols)) {
                this.openCell(row1, col1);
            }
        }
    }
    if (this.nUnknownCells == this.nMines) {
        for (var i = this.nRows - 1; i >= 0; i--) {
            for (var j = this.nCols - 1; j >= 0; j--) {
                var cell = this.cells[i][j];
                if (!cell.isOpen && !cell.isMarked) {
                    this.markCell(i, j);
                }
            }
        }
        this.mode = MODE_WINNER;
        this.stopTimer();
        this.fire();
    }
};

MineSweeperModel.prototype.markCell = function (row, col) {
    var cell = this.cells[row][col];
    if (cell.isOpen) {
        return;
    }
    if (cell.isMarked) {
        cell.isMarked = false;
        this.remainingMinesCounterModel.increment();
    } else {
        cell.isMarked = true;
        this.remainingMinesCounterModel.decrement();
    }
    this.fire();
};

MineSweeperModel.prototype.openNeighboursOf = function (row, col) {
    var cell = this.cells[row][col];
    if (!cell.isOpen) {
        return;
    }
    var nMarkedNeighbours = 0;
    for (var k = 0; k < 8; k++) {
        var row1 = row + DX[k];
        var col1 = col + DY[k];
        if ((row1 >= 0) && (row1 < this.nRows) && (col1 >= 0) && (col1 < this.nCols)) {
            var cell1 = this.cells[row1][col1];
            if (cell1.isMarked) {
                nMarkedNeighbours++;
            }
        }
    }
    if (nMarkedNeighbours == cell.nNeighbours) {
        for (var k = 0; k < 8; k++) {
            var row1 = row + DX[k];
            var col1 = col + DY[k];
            if ((row1 >= 0) && (row1 < this.nRows) && (col1 >= 0) && (col1 < this.nCols)) {
                var cell1 = this.cells[row1][col1];
                if (!cell1.isMarked && !cell1.isOpen) {
                    this.openCell(row1, col1);
                }
            }
        }
    }
};

MineSweeperModel.prototype.setMode = function (mode) {
    if (this.mode == mode) {
        return;
    }
    this.mode = mode;
    this.fire();
};

MineSweeperModel.prototype.setSelectedCell = function (row, col) {
    if ((row == this.selectedRow) && (col == this.selectedCol)) {
        return;
    }
    this.selectedRow = row;
    this.selectedCol = col;
    if (this.mode != MODE_NORMAL) {
        this.fire();
    }
};

MineSweeperModel.prototype.getClockCounterModel = function () {
    return this.clockCounterModel;
};

MineSweeperModel.prototype.getRemainingMinesCounterModel = function () {
    return this.remainingMinesCounterModel;
};

MineSweeperModel.prototype.getSmileyButtonModel = function () {
    return this.smileyButtonModel;
};

MineSweeperModel.prototype.actionPerformed = function () {
    this.reset();
};

// Events

MineSweeperModel.prototype.addListener = function (listener) {
    this.listeners[this.listeners.length] = listener;
};

MineSweeperModel.prototype.removeListener = function (listener) {
    for (var i = this.listeners.length; i >= 0; i--) {
        if (this.listeners[i] == listener) {
            this.listeners[i] = this.listeners[this.listeners.length - 1];
            this.listeners.length--;
            return;
        }
    }
};

MineSweeperModel.prototype.fire = function () {
    for (var i = this.listeners.length - 1; i >= 0; i--) {
        this.listeners[i].modelChanged(this);
    }
};

// Solving

MineSweeperModel.prototype.solve = function () {
    if ((this.mode == MODE_WINNER) || (this.mode == MODE_DEAD)) {
        return;
    }
    while (true) {
        var isFinished = true;
        for (var i = this.nRows - 1; i >= 0; i--) {
            for (var j = this.nCols - 1; j >= 0; j--) {
                var cell = this.cells[i][j];
                if (cell.isOpen && (cell.nNeighbours != 0)) {
                    var nUnknownNeighbours = 0;
                    var nMarkedNeighbours = 0;
                    for (var k = 0; k < 8; k++) {
                        var i1 = i + DX[k];
                        var j1 = j + DY[k];
                        if ((i1 >= 0) && (i1 < this.nRows) && (j1 >= 0) && (j1 < this.nCols)) {
                            var cell1 = this.cells[i1][j1];
                            if (cell1.isMarked) {
                                nMarkedNeighbours++;
                            }
                            if (!cell1.isOpen) {
                                nUnknownNeighbours++;
                            }
                        }
                    }
                    if (nMarkedNeighbours < nUnknownNeighbours) {
                        if (nMarkedNeighbours == cell.nNeighbours) {
                            for (var k = 0; k < 8; k++) {
                                var i1 = i + DX[k];
                                var j1 = j + DY[k];
                                if ((i1 >= 0) && (i1 < this.nRows) && (j1 >= 0) && (j1 < this.nCols)) {
                                    var cell1 = this.cells[i1][j1];
                                    if (!cell1.isMarked && !cell1.isOpen) {
                                        this.openCell(i1, j1);
                                        if ((this.mode == MODE_WINNER) || (this.mode == MODE_DEAD)) {
                                            return;
                                        }
                                    }
                                }
                            }
                            isFinished = false;
                        } else if (nUnknownNeighbours == cell.nNeighbours) {
                            for (var k = 0; k < 8; k++) {
                                var i1 = i + DX[k];
                                var j1 = j + DY[k];
                                if ((i1 >= 0) && (i1 < this.nRows) && (j1 >= 0) && (j1 < this.nCols)) {
                                    var cell1 = this.cells[i1][j1];
                                    if (!cell1.isMarked && !cell1.isOpen) {
                                        this.markCell(i1, j1);
                                    }
                                }
                            }
                            isFinished = false;
                        }
                    }
                }
            }
        }
        if (isFinished) {
            break;
        }
    }
    this.fire();
};
