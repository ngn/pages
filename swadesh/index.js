'use strict'
const qw=x=>x.split(' ')
,til=x=>{const r=Array(x);for(let i=0;i<x;i++)r[i]=i;return r}
,dct=x=>{const r={};for(let i=0;i<x.length;i++)r[x[i][0]]=x[i][1];return r}
,bin=(x,y)=>{for(let i=0;i<x.length;i++)if(y<x[i])return i-1;return x.length}
,esc=x=>x.replace(/[<&'"]/g,x=>ESC['<&\'"'.indexOf(x)]),ESC='&lt; &amp; &apos; &quot;'.split(' ')
,tag=x=>y=>`<${x}>${y}</${x}>`
,thead=tag('thead'),tbody=tag('tbody'),tr=tag('tr'),td=tag('td'),th=tag('th')
,id=x=>document.getElementById(x)
,on=(x,y,z)=>x.addEventListener(y,z)
,off=(x,y,z)=>x.removeEventListener(y,z)
,stop=x=>{x.preventDefault();x.stopPropagation()}
,nw=data[data.length-1].ne.length
Array.prototype.mj=function(f){return this.map(f).join('')}

const rTbl=_=>{
 const c=dct(qw('ipa notes alt').map(x=>[x,id('chk_'+x).checked]))
 tbl.innerHTML=
  thead(tr(data.mj(x=>th(esc(x.nativename||x.l)))))+
  tbody(til(nw).mj(i=>
   tr(data.mj(d=>
    td(til(d.ne[i]).mj(k=>{
     let h='',o=d.oe[i]+k,w=esc(d.term[o]),t='',u=''
     if(k)h+='<span class=sep> | </span>'
     qw('ipa notes alt').forEach(x=>{
       if(d[x]?.[o]){if(c[x]){w+=' '+d[x][o]}else{t+=x.toUpperCase()+`: ${d[x][o]}\n`;u+=x}}})
     h+=t?`<span class=tip title='${esc(t)}'>${w}</span>`:w
     if(u)h+=`<sup>${esc(u)}</sup>`
     return h}))))))}

const reo=t=>{let d,X,Y,h,a,i,j
 const c=x=>{if(i-j&&i-1-j){if(h[j])h[j].style.borderRightColor=x
                            if(h[j+1])h[j+1].style.borderLeftColor=x}}
 ,mv=x=>{d.style.left=x.pageX-X+'px';d.style.top=x.pageY-Y+'px'
         const k=bin(a,x.pageX);if(j-k){c('');j=k;c('#f00')}stop(x)}
 ,up=x=>{c('');const s=d.style;s.position='';s.left=s.right=0;s.cursor='';a=h=d=null;stop(x)
         off(document,'mousemove',mv);off(document,'mouseup',up)
         data.splice(j+(j<i),0,data.splice(i,1)[0]);rTbl()}
 on(t,'mousedown',x=>{if(d||!(d=x.target.closest('th'))||!x.target.closest('thead'))return
  const b=d.getBoundingClientRect(),s=d.style;s.position='relative';s.cursor='move';s.left=s.top=0
  X=x.clientX;Y=x.clientY;h=Array.from(d.parentElement.children);i=j=h.indexOf(d)
  a=h.map(x=>(b=>b.left+b.width/2+.5|0)(x.getBoundingClientRect()))
  on(document,'mousemove',mv);on(document,'mouseup',up);stop(x)})}

onload=_=>{
 data.forEach(d=>{qw('term ipa notes alt').forEach(x=>d[x]&&(d[x]=d[x].split(';')))
  if(!d.ne)d.ne=Array(nw).fill(1)
  d.oe=Array(nw+1);d.oe[0]=0;for(let i=0;i<nw;i++)d.oe[i+1]=d.oe[i]+d.ne[i]})
 qw('ipa notes alt').forEach(x=>on(id('chk_'+x),'click',rTbl));rTbl();reo(tbl)}
