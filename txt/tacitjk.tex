\documentclass{article}\usepackage{array}\setcounter{secnumdepth}0
\newcommand\term[1]{{\em{#1}}}      %terminology
\newcommand\lr\Leftrightarrow       %left-right <=>
\newcommand\jcap{\texttt{\lbrack:}} %j cap [:
\newcommand\kc[1]{\texttt{#1}}      %k code
\newcommand\bs{\char`\\}            %backslash
\newcommand\us{\char`_}             %underscore
\newcommand\f{\mathbf{f}}
\newcommand\g{\mathbf{g}}
\newcommand\h{\mathbf{h}}
\begin{document}\title{Tacit J and K}\author{by ngn}\maketitle

\section{Definitions and Notation}
\term{Tacit programming} is programming without mentioning function arguments explicitly.
Complex functions are constructed from simpler ones through the application of higher-order functions called
\term{combinators}.

\term{Noun-verb syntax} is the basic value-level (as opposed to function-level) syntax used in APL, J, and K.
A verb is applied to the whole expression on its right and, if possible, also to a single noun on its left.
Thus, verbs are prefix monadic $\f x$ or infix dyadic $x\f y$
(by convention we use $x,y,z$ for nouns and $\f,\g,\h\dots$ for verbs even though in K an identifier is always a noun).
Noun-verb syntax can be extended with \term{adverbs} which are postfix and have higher precedence than verb
applications, and \term{conjunctions}---infix and with the same precedence as adverbs.
The application of an adverb or conjunction forms a derived verb.
In APL adverbs and conjunctions are usually called \term{operators}.

A \term{train}, in the context of noun-verb syntax, is a sequence of nouns or verbs (its \term{carriages}) ending with a
verb.
Normally, such a sequence is meaningless, as the last verb has nothing on its right to play the role of right argument,
but if given the semantics of a combinator, it can serve as a convenient mechanism for tacit programming.
All three major array languages implement this idea, but in different ways.

\section{Tacit J}
J's and APL's adverbs and conjunctions are a limited form of higher-order functions and can be used as combinators for
tacit programming.

Additionally, J treats 2- and 3-trains as the \term{hook} and \term{fork} combinators, namely:
\[\begin{array}{c@{}c@{}c@{}c@{\qquad}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{\qquad}c@{}c@{}c@{}c@{}c@{}c}
 ~&(\f\g)y&\lr&y\f(\g y) & ~&(\f\g\h)y&\lr&(& &\f y)\g(& &\h y) &  &(z\g\h)y&\lr&z\g(& &\h y)\\
 x&(\f\g)y&\lr&x\f(\g y) & x&(\f\g\h)y&\lr&(&x&\f y)\g(&x&\h y) & x&(z\g\h)y&\lr&z\g(&x&\h y)
\end{array}\]

Longer trains are reduced, starting from the right, to nested forks, possibly ending with a hook, for example:
\[\mathbf{fghijklm}\lr \mathbf{f(gh(ij(klm)))}\]
We refer to this interpretation of trains as \term{J-style}, or \term{zebra} trains, or \term{odd-even} trains.

Inserting a monadic verb in a J train is inconvenient, so the language supports a special verb-like
token---``\texttt{\lbrack:}'', called \term{cap}, to suppress the left argument of a fork's middle verb:
\[\begin{array}{c@{}c@{}c@{}c@{}c@{}c@{}c@{}c}
 ~&(\jcap\g\h)y &\lr& \g&(& &\h y&)\\
 x&(\jcap\g\h)y &\lr& \g&(&x&\h y&)
\end{array}\]

In the early 2010s Dyalog APL adopted J-style trains, replacing hooks with \term{atops} and eliminating cap:
\[\begin{array}{c@{}c@{}c@{}c@{}c@{}c}
 ~&(\f\g)y&\lr&\f(&~&\g y) \\
 x&(\f\g)y&\lr&\f(&x&\g y)
\end{array}\]

Neither APL nor J assign meaning to trains that have a noun at an odd 0-indexed position starting from the right.
For instance, \texttt{1+} and \texttt{*2-} give syntax errors.

J-style trains have the following drawbacks:
\begin{itemize}
  \item The fork combinator is given the most minimal syntax possible---the 3-train, even though it's not that
   fundamental or commonly needed.
  \item Verbs at odd vs even positions are applied differently---to the argument(s) vs to the results from neighbouring
   verbs:
   \[\begin{array}{cc@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c}
    x(\mathbf{fghijklm})y&\lr&&\f&\rightarrow&\h&\rightarrow&\mathbf{j}&\rightarrow&\mathbf{l}&\\
                &&\swarrow&&&\downarrow&&\downarrow&&\downarrow&\searrow\\
                &x&&&&\g&&\mathbf{i}&&\mathbf{k}&&\mathbf{m}\\
                &&&&&\downarrow\downarrow&&\downarrow\downarrow&&\downarrow\downarrow&&\downarrow\downarrow\\
                &&&&&xy&&xy&&xy&&xy
     \end{array}\]
   It's hard to keep track of that in a longer train, as the factors affecting it lack locality---in order to determine
   how a carriage is applied, the reader must understand and count all carriages to its right.
  \item Fork-hook syntax feels like a sublanguage that doesn't fit in.  Grouping carriages in threes is substantially
   different from ordinary noun-verb syntax and its mirror image---adverb-conjunction syntax.\\
   \begin{tabular}{|l|l|}\hline
    noun-verb grammar          &\tt{e:N|Ve|NVe}\\\hline
    adverb-conjunction grammar &\tt{e:V|eA|eCV}\\\hline
    fork-hook grammar          &\tt{e:Vf|f f:V|VVf}\\\hline
   \end{tabular}
\end{itemize}

\section{Tacit K}
K has multiple incompatible versions.
For simplicity we limit the examples in this section to K6 as implemented in ngn/k.

K functions are first-class values and support up to 8 arguments through M-expression syntax (\texttt{f[x;y;\dots]}).
The simplest way to program tacitly is to implement combinators as ordinary functions. For instance, fork could be
\begin{verbatim}
 {[f;g;h;x] g[f x;h x]}
 {[f;g;h;x;y] g[f[x;y];h[x;y]]}
\end{verbatim}
partially applied to \kc{f}, \kc{g}, and \kc{h}.
The monadic and dyadic versions must be separate because K functions have fixed \term{valence} (number of arguments).

K has trains too, but in contrast with J's, they don't form forks.
A noun-verb combo creates a partial application called \term{projection}:
\[(x\f)y\lr x\f y\]

A pair of verbs (including the noun-verb projections above) forms a \term{composition} in which the left verb is applied
to the result from the right verb.
The composition's valence is the valence of the right verb:
\[\begin{array}{r@{}c@{}l}
 (\f\g)x    &\lr&\f(\g x)\\
 (\f\g)[x;y]&\lr&\f(x\g y)
\end{array}\]

Note that $\f$ and $\g$ here are meant as verbs, not identifiers \kc{f} and \kc{g}.
K's grammar treats all identifiers, parenthesized expressions, and lambdas as nouns.
They cannot be applied infix, but dyadic application is possible through an M-expression like $(\f\g)[x;y]$.

Longer trains follow the same rules right-to-left, so all carriages except the last are interpreted as monadic.
The last carriage determines the valence of the entire train.
Some examples of K trains are:
\[\begin{array}{c}
 \kc{(1+)x} \lr \kc{1+x}\\
 \kc{(-*)x} \lr \kc{-*x}\\
 \kc{(\%*)[x;y]} \lr \kc{\%x*y}\\
 \kc{(1-\us\%*)[x;y]} \lr \kc{1-\us\%*[x;y]} \lr \kc{1-\us\%x*y}
\end{array}\]
The syntax of a K train matches the syntax of ordinary K expressions, so its application boils down to removing the
parentheses around the train.

K can express a fork through the train
\[\kc{g/(f;h)@\bs:}\]
where \kc{(f;h)} is a list of the functions \kc{f} and \kc{h},\\
\kc{@} is the ``apply'' verb,\\
\kc{\bs:} is the ``each left'' adverb,\\
and \kc{g/} is ``fold''---\kc{g} applied over the pair of results from \kc{f} and \kc{h}.

Hook is
\[\kc{f/1 g\bs}\]
where \kc{1 g\bs} is one iteration of \kc{g} over the argument, i.e. the pair \kc{(x;g x)},
and \kc{f/} applies \kc{f} between \kc{x} and \kc{g x}

\end{document}
