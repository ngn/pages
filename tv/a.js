'use strict';const min=(x,y)=>Math.min(x,y),max=(x,y)=>Math.max(x,y),id=x=>document.getElementById(x),esc=x=>x.replace(/[<&"']/g,x=>ESC[x]||x),ESC={'<':'&lt;','&':'&amp;','"':'&quot;',"'":'&apos;'}
,now=_=>performance.now(),dlgt=(x,y,z,f)=>x.addEventListener(y,e=>{for(var t=e.target;t&&t!=x;t=t.parentNode)if(t.matches&&t.matches(z))return f(e,t)},0)
,rdy=x=>document.readyState!=='loading'?x():document.addEventListener('DOMContentLoaded',x),bin=(x,y)=>{let i=-1,j=x.length,m;while(i+1<j)y<x[m=i+j>>1]?j=m:(i=m);return i}
,AP=_=>{let vt,nh,nw,dur=0,raf=0,coTId=0,tUIId=0,blIId=0,blink=1,hold=0,st='initial',frs=[],tid=0,frI=0,virT=0,t0,pauT //asciinema-player
  const ew=id('ap_w'),eg=id('ap_g'),ap_timer=id('ap_timer'),es=id('ap_s'),et=id('ap_t'),evh={play:[],pause:[],seeked:[],ended:[]},on=(x,y)=>evh[x].push(y),fire=x=>evh[x].forEach(x=>x())
  ,feed=x=>{vt.feed(x);raf=raf||requestAnimationFrame(uTerm)}
  ,nxt=_=>{let f=frs[frI],t;do{feed(f[1]);virT=f[0]*1e3;f=frs[++frI];t=now()-t0}while(f&&(t>f[0]*1e3));sch()}
  ,sch=_=>{if(frI<frs.length){tid=setTimeout(nxt,max(0,frs[frI][0]*1e3+t0-now()))}else{fire('ended');pause();seek(0)}}
  ,getT=_=>(tid?now()-t0:pauT??0)/1e3
  ,pause=_=>{if(tid){clearTimeout(tid);tid=0;pauT=now()-t0;bl0();tu0();ew.classList.remove('playing');fire('pause')}}
  ,play=_=>{es.style.display='none';if(!tid){t0=now()-pauT;pauT=null;sch();bl1();tu1();ew.classList.add('playing');fire('play')}}
  ,pp=_=>tid?pause():play()
  ,tu1=_=>{tu0();tUIId=setInterval(uTimes,100)},tu0=_=>clearInterval(tUIId)//start/stop time updates
  ,bl1=_=>blIId=setInterval(_=>{if(blink^=1)hold=0;uTermCl()},500),bl0=_=>{clearInterval(blIId);blink=1;uTermCl()}//start/stop blinking
  ,seek=x=>{if(tid){pause();seek(x);play();return}
    const t=max(0,min(dur,x))*1e3;if(t<virT){feed('\x1bc');frI=0;virT=0}let f=frs[frI];while(f&&(f[0]*1e3<t)){feed(f[1]);virT=f[0]*1e3;f=frs[++frI]}pauT=t;uTimes();co(1);fire('seeked')}
  ,cc=(c,i,p)=>typeof c==='number'?`${p}${i&&c<8?c+8:c}`:c==='fg'||c==='bg'?`${p}${c}`:null//colourClass(c:colour,i:intense,p:prefix)
  ,lc=(x,extra)=>{const i=x.get('inverse'),fg=i?(x.has('bg')?x.get('bg'):'bg'):x.get('fg'),bg=i?(x.has('fg')?x.get('fg'):'fg'):x.get('bg')//line class
    const r=[extra,cc(fg,x.get('bold'),'fg-'),cc(bg,x.get('blink'),'bg-')].filter(x=>x).concat(['bold','italic','underline','blink'].filter(y=>x.has(y))).join(' ');return r&&` class='${r}'`}
  ,ls=x=>{let fg=x.get('fg'),bg=x.get('bg'),r;if(x.get('inverse'))[fg,bg]=[bg,fg]//line style
    r=(typeof fg==='string'?`color:${fg};`:'')+(typeof bg==='string'?`background-color:${bg};`:'');return r&&` style='${r}'`}
  ,line=(segs,cx)=>{
    if(cx>=0){let n=0,i=0;while(i<segs.length&&n+segs[i][0].length<=cx)n+=segs[i++][0].length;let r=segs.slice(0,i);
      if(i<segs.length){const s=segs[i],a=s[1],b=new Map(a),p=cx-n;b.set('inverse',!b.get('inverse'));p>0&&r.push([s[0].slice(0,p),s[1]]);r.push([s[0][p],a,' cursor-a'],[s[0][p],b,' cursor-b'])
        p<s[0].length-1&&r.push([s[0].slice(p+1),s[1]]);r=r.concat(segs.slice(i+1))}
      segs=r}
    return`<span class=line>${segs.map(([x,y,z])=>`<span${lc(y,z)}${ls(y)}>${esc(x)}</span>`).join('')}</span>`}
  ,uTermLines=_=>{let h='';const[cx,cy]=vt.get_cursor()||[-1,-1];for(let i=0;i<nh;i++)h+=line(vt.get_line(i),i===cy?cx:-1);et.innerHTML=h}
  ,uTermCl=_=>{const l=et.classList;l.toggle('cursor',blink||hold);l.toggle('blink',blink)}
  ,uTerm=_=>{hold=1;uTermCl();uTermLines();raf=0}
  ,uWH=_=>{const s=et.style;s['font-size']=document.fullscreenElement?min(ew.offsetWidth/et.offsetWidth,ew.offsetHeight/et.offsetHeight)*100+'%':'';s.width=nw+'ch';s.height=1.3*nh+'em'}
  ,fmt=x=>isNaN(x)?'--:--':x<0?'-'+fmt(-x):[x/60,x%60].map(x=>('0'+~~x).slice(-2)).join(':')
  ,uTimes=_=>{const t=getT();ap_timer.textContent=fmt(ap_timer.matches(':hover')?min(0,t-dur):t);eg.style.transform=`scaleX(${min(1,t/dur)||0})`}
  ,co=x=>{clearTimeout(coTId);if(x)coTId=setTimeout(_=>co(0),2000);ew.classList.toggle('hud',x)}//show controls
  ,fs=_=>{ew.classList.toggle('fs');document.fullscreenElement?document.exitFullscreen():ew.requestFullscreen()}
  ,load=async x=>{pause();seek(0);const r=await fetch(x+'.cast');if(!r.ok)throw'fetch'
    const a=(await r.text()).split('\n').filter(x=>x).map(x=>JSON.parse(x));if(a[0].version!==2)throw'version'
    const b=a.slice(1).filter(x=>x[1]==='o').map(x=>[x[0],x[2]]);dur=b[b.length-1][0];nw=a[0].width;nh=a[0].height
    frs=[];let i=0;while(i<b.length){const p=b[i++];frs.push(p);while(i<b.length&&b[i][0]-p[0]<1/60)p[1]+=b[i++][1]}
    if(!vt||vt.cols!==nw||vt.rows!==nh)vt=(await VT).create(nw,nh);vt.cols=nw;vt.rows=nh;uWH();uTermLines()}
  id('ap_seek').onmousedown=x=>x.altKey||x.shiftKey||x.metaKey||x.ctrlKey||seek((x.clientX-x.target.getBoundingClientRect().left)/x.target.offsetWidth*dur);new ResizeObserver(uWH).observe(ew)
  ap_timer.onmouseover=ap_timer.onmouseout=uTimes;id('ap_player').onmousemove=_=>co(1);id('ap_pause').onclick=es.onclick=_=>(pp(),!1);id('ap_fs').onclick=_=>(fs(),!1)
  return{getT,on,play,pp,load,seek,fs}}

,Au=p=>{const e=new Audio;p.on('play',_=>e.play());p.on('pause',_=>e.pause());p.on('seeked',_=>e.currentTime=p.getT())
  return{load:x=>{e.src=x+'.ogg';e.currentTime=0;return new Promise(f=>e.addEventListener('canplaythrough',f))},tglMute:_=>e.muted^=1,vol:x=>e.volume=max(0,min(1,e.volume+x))}}
,Sch=(p,f)=>{let t=[],v=[],i=0,tid;const sch=_=>tid=i<t.length?setTimeout(nxt,1e3*(t[i]-p.getT())):0,nxt=_=>{f(v[i],i);i++;sch()}
  p.on('play',sch);p.on('pause',_=>tid=clearTimeout(tid));p.on('seeked',_=>{i=bin(t,p.getT());f(v[i],i);i++})
  return{set:(x,y)=>{t=x;v=y},jmp:x=>{let i=bin(t,p.getT())+x;p.seek(t[max(0,min(t.length-1,i))])}}}
,Sub=p=>{const e=id('sub'),f=x=>e.textContent=x||'',o=Sch(p,f),Q=x=>{if(!x)throw'srt'}
  ,pt=x=>{if(!/^\d\d:\d\d:\d\d,\d\d\d$/.test(x))throw`srt`;let r=0;for(let i=0;i<PT.length;i++)if(PT[i])r+=PT[i]*+x[i];return r},PT=[36e3,3600,0,600,60,0,10,1,0,.1,.01,.001]//parse timestamp
  return{load:async x=>{const t=[0],s=[''];o.set([],[]);f();const r=await fetch(x+'.srt');if(!r.ok)return
    ;(await r.text()).replace(/\n+$/,'').split(/\n\n+/g).forEach((x,i)=>{x=x.split(/\n/g);Q(x.length>2||+x[0]===i+1);const v=x[1].split(' --> ');Q(v.length===2);
      const[t0,t1]=v.map(pt);Q(t0<t1);Q(t[t.length-1]<=t0);if(!s[t.length-1]&&t[t.length-1]===t0){t.pop();s.pop()}t.push(t0,t1);s.push(x.slice(2).join('\n'),'')});o.set(t,s)}}}
,ToC=p=>{let se;const e=id('toc'),f=(x,i)=>{se&&se.classList.remove('sel');se=e.children[i];se&&se.classList.add('sel')},o=Sch(p,f)
  const pms=x=>{if(!/^\d\d:\d\d\s/.test(x))throw'toc';return 600*+x[0]+60*+x[1]+10*+x[3]+(+x[4])}//parse "mm:ss"
  return{jmp:o.jmp,load:async x=>{e.innerHTML='';o.set([],{});const r=await fetch(x+'.toc');if(!r.ok)return
                                  const a=(await r.text()).replace(/\n+$/,'').split(/\n/g),t=a.map(pms);e.innerHTML=a.map((y,i)=>`<a href='#v${x}t${t[i]}'>${esc(y)}</a>`).join('');o.set(t,{})}}}
rdy(_=>{const p=AP(),au=Au(p),sub=Sub(p),toc=ToC(p),tit=['Setting up ngn/k'];let vI=0,v=id('vids')
  const ld=async i=>{v.children[vI]?.classList.remove('sel');vI=i;v.children[vI].classList.add('sel');await sub.load(i);await toc.load(i);await au.load(i);await p.load(i)}
  v.innerHTML=tit.map((x,i)=>`<a href='#v${i}t0'>${esc(x)}</a>`).join('');id('ap_w').focus()
  dlgt(document,'click','a',async(_,x)=>{const m=/^#v(\d+)t(\d+)$/.exec(x.getAttribute('href')||'');if(!m)return
    const[v,t]=[+m[1],+m[2]];if(v!==vI)await ld(+h.replace(/\D/g,''));p.seek(t);p.play();id('ap_w').focus()})
  const SK=x=>_=>p.seek(p.getT()+x),TG=x=>_=>id(x).hidden^=1
  const km={' ':p.pp,f:p.fs,'ArrowLeft':SK(-5),'ArrowRight':SK(5),'S-ArrowLeft':SK(-60),'S-ArrowRight':SK(60),F2:TG('help'),'S-?':TG('help'),h:TG('help'),s:TG('sub'),v:TG('vids'),t:TG('toc'),
   m:au.tglMute,ArrowUp:_=>au.vol(.1),ArrowDown:_=>au.vol(-.1),'C-ArrowLeft':_=>toc.jmp(-1),'C-ArrowRight':_=>toc.jmp(1)}
  document.onkeydown=x=>{if(x.altKey||x.metaKey)return;let k=(x.ctrlKey?'C-':'')+(x.shiftKey?'S-':'')+x.key;if(km[k]){km[k]();return!1}}
  id('helpbtn').onclick=_=>(km.h(),!1)
  ld(0)})
