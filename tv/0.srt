1
00:00:00,500 --> 00:00:02,000
Hello. In this video

2
00:00:02,000 --> 00:00:04,000
I'm gonna show how to set up

3
00:00:04,000 --> 00:00:06,000
a working environment for ngn/k

4
00:00:06,000 --> 00:00:08,000
similar to the one I use

5
00:00:09,000 --> 00:00:11,000
I'll assume essential developer tools

6
00:00:11,000 --> 00:00:13,000
have already been installed

7
00:00:13,500 --> 00:00:15,000
such as the text editor Vim

8
00:00:15,000 --> 00:00:18,000
a C compiler - either clang or gcc

9
00:00:18,000 --> 00:00:21,000
the GNU flavour of "make"

10
00:00:21,000 --> 00:00:22,500
a git client

11
00:00:22,500 --> 00:00:25,000
and optionally the rlwrap utility

12
00:00:25,000 --> 00:00:27,500
which I'm gonna talk about in a minute

13
00:00:28,000 --> 00:00:29,500
The easiest way for a programmer

14
00:00:29,500 --> 00:00:31,500
to download the source code is with git

15
00:00:32,500 --> 00:00:34,000
It's hosted on codeberg

16
00:00:39,000 --> 00:00:40,500
Let's take a look inside

17
00:00:41,000 --> 00:00:42,500
The C source code is organized

18
00:00:42,500 --> 00:00:44,000
compactly in a bunch of files

19
00:00:44,000 --> 00:00:46,000
with single-letter names

20
00:00:46,000 --> 00:00:47,500
and cryptic dense content

21
00:00:47,500 --> 00:00:49,000
unreadable to the uninitiated

22
00:00:49,500 --> 00:00:51,000
But let's ignore that

23
00:00:51,000 --> 00:00:53,000
and just build the project by typing "make"

24
00:00:54,500 --> 00:00:57,000
This also runs the various test suites

25
00:00:58,000 --> 00:01:00,000
In order to only build the executable

26
00:01:00,000 --> 00:01:03,000
you should type "make k"

27
00:01:03,000 --> 00:01:07,000
or simply press <C-c> to stop the tests

28
00:01:08,000 --> 00:01:10,000
Running the interpreter without arguments

29
00:01:10,000 --> 00:01:12,000
starts what we call a "REPL" -

30
00:01:12,000 --> 00:01:14,500
a "Read-Eval-Print Loop"

31
00:01:15,000 --> 00:01:17,500
Passing a filename would have made it

32
00:01:17,500 --> 00:01:19,500
evaluate the file instead

33
00:01:20,500 --> 00:01:22,500
In the REPL, typing a K expression

34
00:01:22,500 --> 00:01:26,000
like "1 2+3" and pressing <Enter>

35
00:01:26,000 --> 00:01:27,500
causes the interpreter to parse that,

36
00:01:27,500 --> 00:01:29,500
evaluate it, output the result,

37
00:01:29,500 --> 00:01:31,500
and wait for the next one

38
00:01:32,500 --> 00:01:35,500
By default it reads directly from stdin

39
00:01:35,500 --> 00:01:39,000
so, it doesn't have any line editing conviences,

40
00:01:39,000 --> 00:01:41,500
or history, or help

41
00:01:42,000 --> 00:01:46,500
To quit the REPL, press <C-d> or type \\

42
00:01:47,000 --> 00:01:48,000
Let's try to make the interaction

43
00:01:48,000 --> 00:01:49,000
slightly more pleasant

44
00:01:49,000 --> 00:01:52,000
by using a program called "rlwrap"

45
00:01:52,500 --> 00:01:55,500
It gives you the opportunity to edit a line comfortably

46
00:01:55,500 --> 00:01:57,500
before sending it to the interpreter with <Enter>

47
00:01:57,500 --> 00:02:00,000
For instance, you can use

48
00:02:00,000 --> 00:02:02,500
<Left> and <Right> arrows to move around,

49
00:02:02,500 --> 00:02:05,000
<C-a> to go to the beginning of a line,

50
00:02:05,000 --> 00:02:07,000
<C-w> to cut the previous word, and so on -

51
00:02:07,000 --> 00:02:09,000
the kind of shortcuts that may be

52
00:02:09,000 --> 00:02:11,000
familiar to you from Emacs or

53
00:02:11,000 --> 00:02:13,000
from bash by default

54
00:02:13,500 --> 00:02:16,000
It also keeps a persistent record

55
00:02:16,000 --> 00:02:18,000
of what you typed

56
00:02:18,500 --> 00:02:20,500
use <Up> and <Down> arrows to go through its history

57
00:02:20,500 --> 00:02:22,500
and <C-r> to search in it

58
00:02:23,000 --> 00:02:25,500
It's handy to have an alias for the REPL

59
00:02:26,000 --> 00:02:29,000
So, I'm gonna append it to the end of my bashrc

60
00:02:33,500 --> 00:02:38,000
alias k='rlwrap ~/k/k'

61
00:02:38,000 --> 00:02:40,500
where ~ expands to your home directory

62
00:02:42,000 --> 00:02:44,500
I'm also gonna source the bashrc file with dot

63
00:02:44,500 --> 00:02:47,000
to make changes take effect immediately

64
00:02:49,000 --> 00:02:52,000
Let's make sure that worked

65
00:02:52,000 --> 00:02:53,000
there we go

66
00:02:53,500 --> 00:02:55,000
The REPL is nice

67
00:02:55,000 --> 00:02:56,500
for quickly trying things out

68
00:02:56,500 --> 00:02:58,000
but most of the time you'd want

69
00:02:58,000 --> 00:02:59,500
to write code in files

70
00:02:59,500 --> 00:03:02,000
because, well, they are persistent

71
00:03:02,500 --> 00:03:05,000
and can be put under version control

72
00:03:05,500 --> 00:03:07,500
Let's create a file...

73
00:03:09,500 --> 00:03:11,000
Passing the filename as argument

74
00:03:11,000 --> 00:03:13,500
to the k interpreter will run it

75
00:03:14,000 --> 00:03:16,500
There's a convention for such scripts to

76
00:03:16,500 --> 00:03:19,000
state their own interpreter on the first line

77
00:03:19,000 --> 00:03:21,500
with a so-called "hashbang"

78
00:03:24,000 --> 00:03:26,500
Then, if we make the file executable

79
00:03:26,500 --> 00:03:28,500
with "chmod +x"

80
00:03:28,500 --> 00:03:30,500
and try to run the file directly

81
00:03:30,500 --> 00:03:33,000
the kernel will know what to do

82
00:03:33,000 --> 00:03:35,000
to make it run

83
00:03:35,500 --> 00:03:38,500
since we're gonna be running the script often

84
00:03:38,500 --> 00:03:40,500
as we're developing code

85
00:03:40,500 --> 00:03:42,500
it would pay off to set up

86
00:03:42,500 --> 00:03:45,000
a short keybinding for that in Vim

87
00:03:45,500 --> 00:03:48,000
So, let's create a ".vimrc"

88
00:03:48,500 --> 00:03:51,500
and put a normal-mode non-recursive mapping

89
00:03:51,500 --> 00:03:54,000
for instance for the <Enter> key

90
00:03:54,500 --> 00:03:56,500
that writes all open files,

91
00:03:56,500 --> 00:03:59,000
makes sure the current file is executable

92
00:03:59,000 --> 00:04:01,000
and runs it

93
00:04:01,000 --> 00:04:03,500
% here stands for the current filename

94
00:04:05,000 --> 00:04:07,000
Let's try it...

95
00:04:07,000 --> 00:04:09,500
As you can see, it gives us a

96
00:04:09,500 --> 00:04:11,000
short edit-test cycle

97
00:04:11,000 --> 00:04:13,500
convenient for development

98
00:04:17,000 --> 00:04:20,000
Now, let's try to organize things a bit better

99
00:04:20,000 --> 00:04:22,500
This relative path in the hashbang

100
00:04:22,500 --> 00:04:24,500
would work only if we are

101
00:04:24,500 --> 00:04:26,500
in the directory we are in now -

102
00:04:26,500 --> 00:04:28,500
the home directory

103
00:04:28,500 --> 00:04:30,500
I could replace it with an absolute path

104
00:04:30,500 --> 00:04:33,500
but it's much better to avoid hard-coding it

105
00:04:34,000 --> 00:04:36,000
The standard way to make binaries available

106
00:04:36,000 --> 00:04:38,000
as commands everywhere

107
00:04:38,000 --> 00:04:41,500
is to put them on the $PATH environment variable

108
00:04:42,500 --> 00:04:45,500
/usr/bin/env will search there

109
00:04:47,500 --> 00:04:50,000
Now let's edit bashrc again

110
00:04:51,500 --> 00:04:54,000
and add to the PATH variable

111
00:04:54,000 --> 00:04:57,000
":" - used as a separator

112
00:04:57,000 --> 00:04:59,000
and "~/k" -

113
00:04:59,000 --> 00:05:03,000
the directory that contains the interpreter

114
00:05:03,500 --> 00:05:05,500
Don't forget to source it

115
00:05:06,000 --> 00:05:08,500
and test it

116
00:05:10,000 --> 00:05:11,500
much better

117
00:05:12,500 --> 00:05:15,000
Note that k being on the PATH

118
00:05:15,500 --> 00:05:17,500
doesn't conflict with the k alias

119
00:05:17,500 --> 00:05:20,000
we set up earlier for the REPL

120
00:05:20,000 --> 00:05:22,000
and, when interacting with bash,

121
00:05:22,000 --> 00:05:24,000
the alias takes precedence

122
00:05:25,500 --> 00:05:27,500
OK, time to make another improvement

123
00:05:27,500 --> 00:05:29,500
to the REPL

124
00:05:30,000 --> 00:05:33,500
ngn/k comes with a file called repl.k

125
00:05:34,000 --> 00:05:36,500
If you pass that file to the interpreter,

126
00:05:36,500 --> 00:05:39,000
it starts a friendlier REPL

127
00:05:39,000 --> 00:05:42,000
with some niceties like a single-space prompt,

128
00:05:42,000 --> 00:05:44,500
a banner, reminding you about the licence,

129
00:05:44,500 --> 00:05:48,000
and access to some concise help about the language

130
00:05:50,000 --> 00:05:52,000
Let's edit bashrc once again

131
00:05:52,000 --> 00:05:54,000
and update the k alias

132
00:05:54,000 --> 00:05:56,500
with this improved REPL

133
00:06:01,000 --> 00:06:03,500
The last thing I'd like to show now

134
00:06:03,500 --> 00:06:06,500
is how to configure syntax highlighting

135
00:06:07,000 --> 00:06:09,500
There's a directory in the project's root

136
00:06:09,500 --> 00:06:12,000
called "vim-k"

137
00:06:12,000 --> 00:06:14,500
Its structure is similar to that of ".vim" -

138
00:06:14,500 --> 00:06:17,000
the standard layout vim uses

139
00:06:17,500 --> 00:06:20,000
so, it's suitable for appending

140
00:06:20,000 --> 00:06:22,500
to what Vim calls the "runtime path"

141
00:06:23,500 --> 00:06:26,000
I'm gonna do that in .vimrc now

142
00:06:26,500 --> 00:06:29,000
There's a bit of a magical incantation

143
00:06:29,000 --> 00:06:31,500
you have to type here

144
00:06:33,000 --> 00:06:37,000
This means: set "no compatible",

145
00:06:37,000 --> 00:06:41,000
then, "runtime path" append our vim-k directory

146
00:06:41,000 --> 00:06:43,500
and turn syntax highlighting on

147
00:06:46,500 --> 00:06:49,000
so, let's see...

148
00:06:49,500 --> 00:06:51,500
Now you have colours for

149
00:06:51,500 --> 00:06:54,000
the syntactic elements of K

150
00:06:54,500 --> 00:06:57,500
different colours for monadic and dyadic verbs

151
00:06:57,500 --> 00:07:00,500
another colour for adverbs, and so on

152
00:07:01,500 --> 00:07:04,000
That's all for this video, I'm afraid

153
00:07:04,500 --> 00:07:08,000
Thanks for watching
